#!/usr/bin/env Rscript

# calculate simple histograms, print to stdout

suppressPackageStartupMessages(library(optparse))
suppressPackageStartupMessages(library(dplyr))

# define options
usage <- "usage: histogram.R [options]"
description <- "CLI utility to print histograms to stdout"
option_list = list(
  make_option(c("-d", "--delimiter"),
  				    action = "store",
  				    type = "character",
  				    default= "|",
  				    help = "Input file delimiter [default: %default]"),
	make_option(c("-n", "--numeric"),
				      action = "store_true",
				      default = FALSE,
				      help="Sort histogram by descending values [default: sort alphabetically by key]"),
  make_option(c("-x", "--xvar"),
              action = "store",
              default = 1,
              type = "integer",
              help = "Index of column to create histogram [default: %default]")
  )

# parse options
opt = parse_args(OptionParser(usage = usage, description = description, option_list=option_list))

# import data
d <- data.table::fread('cat /dev/stdin', sep = opt$d, data.table = F, showProgress = F)

# parse x variable
var <- names(d)[opt$x]

# compute histogram
out <- d %>% dplyr::select_(var) %>%
  group_by_(var) %>%
  count() %>%
  ungroup()

# sort if option is supplied
if(opt$n) {
  out <- out %>% arrange(desc(n))
} else {
  out <- out %>% arrange_(var)
}

# add additional output columns
out <- out %>%
  mutate(pct = n/sum(n), cume_pct = cumsum(n)/sum(n))

# print to stdout
write.table(out, file = stdout(), quote = F, sep = "|",
            na = "", row.names = F)
