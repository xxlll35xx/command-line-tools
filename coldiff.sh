#!/bin/bash

# Side by side diff
# with coloration.

diff -W 220 -y $@ | colordiff | less -R
