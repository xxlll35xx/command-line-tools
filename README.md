# Command Line Tools for Data Analysis

Simple command line scripts for common analytics tasks.  Once downloaded, place this repo on your path to access these utilties.

All scripts read from stdin and print to stdout.  This makes them ideal for use in Linux pipelines.

Scripts are predominantly written in Python, R, and bash.  Python and R scripts may require the installation of non-base packages/libraries.  All imports on described at the beginning of each script.

### Scripts
colnames:  Print the index and value of the first row (i.e., the header) of a file.
```
Usage:
	colnames FILE DELIMITER
	cat FILE | colnames - DELIMITER
```

histogram.R:  Print a histogram (category, count, pct, cumulative pct) of one column in a file
```
Usage:  cat FILE | histogram.R -x COLUMN INDEX
```
