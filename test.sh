#!/bin/bash

# from http://stackoverflow.com/questions/6980090/how-to-read-from-file-or-stdin-in-bash

#while read line
#do
#	echo "$line"
#done < "${1:-/dev/stdin}"

read line < "${1:-/dev/stdin}"
cat $line | awk -F$2 'NR == 1 {for(i = 1; i <= NF; i++) {print i, "\t", $i}}'

